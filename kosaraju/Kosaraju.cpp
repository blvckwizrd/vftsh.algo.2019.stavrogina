﻿#include <iostream>
#include<unordered_map>
#include <unordered_set>
#include<stack>
#include<map>
struct Vertex {
    int data, color = 0;
    std::unordered_set<int> neighbors;
};
class Kosaraju {
    std::unordered_map<int, Vertex> graph, inversion; // граф и инвертированный граф
    std::multimap<int, int> out; // time and vertex
    std::stack<int> vertices, normal; //vertices - стек с вершинами, отсортированными по времени, normal - тсек с вершинами из graph
    int time = 0;
    void dfs(Vertex& vertex, std::unordered_map<int, Vertex>& copy, std::stack<int>& stack) {
        vertex.color = 1;
         for (auto it = vertex.neighbors.begin(); it != vertex.neighbors.end(); ++it) {
            auto key = copy.find(*it);
             if (key->second.color == 0) 
             dfs(key->second, copy, stack);
         }
         out.insert(std::make_pair(time, vertex.data));
         vertex.color = 2;
         ++time;
         stack.pop();
    }
    int search() {
        for (;normal.size() != 0;)
        dfs(graph.find(normal.top())->second, graph, normal);
        for (auto it = out.begin(); it != out.end(); ++it) {
            vertices.push(it->second);
        }
        int component = 0;
        for (; vertices.size() != 0;) {
            dfs(inversion.find(vertices.top())->second, inversion, vertices);
            ++component;
        }
        return component;
    }
    void pushVertex(int vertex, int count) {
            Vertex El;
            El.data = vertex;
            normal.push(vertex);
            pushInversion(El, vertex);
            for (int i = 0; i < count; ++i) {
                int neighbor;
                std::cin >> neighbor;
                if (neighbor != vertex) {
                    El.neighbors.insert(neighbor);
                    pushInversion(El, neighbor);
                }
            }
            graph.insert(std::make_pair(vertex, El));
    }
    void pushInversion(Vertex&El, int neighbor) {
        Vertex inversEl;
        if (inversion.find(neighbor) == inversion.end()) {
            inversEl.data = neighbor;
            if (neighbor != El.data) 
                inversEl.neighbors.insert(El.data);
            inversion.insert(std::make_pair(neighbor, inversEl));
        }
        else if (neighbor != El.data)
                inversion.find(neighbor)->second.neighbors.insert(El.data);
    }
public:
    int StrongComponent() {
        return search();
    }
    void Show() {
        std::cout << graph.size() << '\n';
        for (auto it = graph.begin(); it != graph.end(); ++it) {
            std::cout << "key in map graph: " << it->first << " data: " << it->second.data << " color: " << it->second.color << '\n';
            for (auto k = it->second.neighbors.begin(); k != it->second.neighbors.end(); ++k) std::cout << " neighbor:  " << *k << '\n';
        }
        std::cout << "inversion graph: " << '\n';
        std::cout << inversion.size() << '\n';
        for (auto it = inversion.begin(); it != inversion.end(); ++it) {
            std::cout << "key in map graph: " << it->first << " data: " << it->second.data << " color: " << it->second.color << '\n';
            for (auto k = it->second.neighbors.begin(); k != it->second.neighbors.end(); ++k) std::cout << " neighbor:  " << *k << '\n';
        }
    }
    void Push(int vertex) {
        std::cout << "enter count and neighbors: " << '\n';
        int count;
        std::cin >> count;
        pushVertex(vertex, count);
    }
};
int main() {
    Kosaraju h;
    int count;
    std::cout << "enter number of vertices: " << '\n';
    std::cin >> count; 
    for (int i = 0; i < count; ++i) {
        int vertex;
        std::cout << "enter vertex: " << '\n';
        std::cin >> vertex;
        h.Push(vertex);

    }
    h.Show();
    std::cout << "strong connectivity component: " <<  h.StrongComponent() << '\n';
}

