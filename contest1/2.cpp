
#include <iostream>
#include <deque>

int main() {
	std::deque<int> numbers;
	int n;
	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		int j;
		std::cin >> j;
		numbers.push_back(j);
	}
	while (numbers.size() > 1) {
		int a = numbers.front();
		numbers.pop_front();
		int b = numbers.front();
		numbers.pop_front();
		if ((a + b) % 2 == 0) {
			numbers.push_back(a + b);
		}
		else {
			numbers.push_back(a - b);
		}
	}
	std::cout << numbers.front();
	return 0;
}

