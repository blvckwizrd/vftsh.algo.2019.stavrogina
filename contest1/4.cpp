#include<iostream>
#include<vector>
#include<algorithm>

void swap(int* k, int* j) {
	int z = *k;
	*k = *j;
	*j = z;
};

int main() {
	std::vector<int> array;
	int n, sum1 = 0, sum2 = 0;
	std::cin >> n;
	for (int i = 0; i < n * 2; ++i) {
		int j;
		std::cin >> j;
		if (i < n) {
			sum1 += j;
		}
		if (i >= n) {
			sum2 += j;
		}
		array.push_back(j);
	}
	if (sum1 != sum2) {
		for (int i = 0; i < 2 * n; ++i) {
			std::cout << array[i] << " ";
		}
		return 0;
	}
	if (sum1 == sum2 && n == 1) {
		std::cout << "-1";
		return 0;
	}
	if(sum1 == sum2 && n > 1) {
		std::sort(array.begin(),array.begin() + n); // или n - 1?
		std::sort(array.begin() + n, array.end());
		int i = 0, k = n;
		//for (int k = n; k < 2 * n && array[i] == array[k]; ++k) {
		for (int i = 0; i < n && array[i] == array[k]; ++i);
		if (i == n - 1) {
			for (k = ++n; array[k] == array[n]&& k < 2 * n; ++k);
		}
		/*while (array[i] == array[k] && k < 2 * n - 1) {
			if (i == n) {
				i = 0;
				++k;
			}
			++i;
		}*/
		if (array[i] != array[k]) {
			swap(&array[i], &array[k]);
			for (i = 0; i < 2 * n; ++i) {
				std::cout << array[i] << " ";
			}
		}
		else {
			std::cout << "-1";
		}
	}
	return 0;
}