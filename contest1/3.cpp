#include <iostream>
#include <vector>
#include <algorithm>

int main() {
	std::vector<int> array;
	int n, even, odd;
	std::cin >> n;
	for (int i; i < n; ++i) {
		int j;
		std::cin >> j;
		array.push_back(j);
		if (j % 2 == 0) {
			even++;
		}
		else {
			odd++;
		}
	}
	if (odd == 0 || even == 0) {
		int i = 0;
		while (i < n) {
			std::cout << array[i] << std::endl;
			++i;
		}
	}
	else {
		std::sort(array.begin(), array.end());
		int i = 0;
		while (i < n) {
			std::cout << array[i] << std::endl;
			++i;
		}

	}
	return 0;
}
