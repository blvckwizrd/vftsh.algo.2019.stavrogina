﻿
#include <iostream>
#include <vector>
#include <algorithm>

class TBinSearch {
public:
	int finder(std::vector<int> vector, int key) {
		//std::sort(vector.begin(), vector.end()); нужно ли нам сортировать массив, или мы считаем, что пользователь дает уже отсортированный?
		return sort(vector, 0, vector.size() - 1, key);
	}
private:
	int sort(std::vector<int> vector, int l, int r, int key) {
		if (r - l > 0) {
			if (key < vector[(r - l) / 2]) {
				sort(vector, l, (r - l) / 2 - 1, key);
				//std::cout << "you here" << r  << " " << l << std::endl;
			}
			if (key > vector[(r - l) / 2]) {
				sort(vector, (r - l) / 2 + 1, r, key);
				//std::cout << "you here 1 " << r << " " << l << std::endl;
			}
			if (key == vector[(r - l) / 2]) {
				//std::cout << "here" << r << " " << l << std::endl;
				return (r - l) / 2;
			}
		}
		else if (key == vector[r]) {
			std::cout << "now here" << std::endl;
			return r;
		}
	}
		//else if (key == vector[l]) {
			//return l;
		//}
		//else if (key == vector[r]) {
			//return r;
		//}
		//else return -1; // не знаю нужно ли, пусть для моего примера будет, могу убрать */
};

int main() {
	std::vector<int> array;
	int k = 1;
	for (int i = 0; i < 7; ++i){
		array.push_back(k);
		k = k + 2;
	}
	std::cout << array.size() << std::endl;
	for (int i = 0; i < array.size(); ++i) {
		std::cout << array[i] << " ";
	}
	TBinSearch h;
	std::cout << std::endl;
	//std::cout << std::endl;
	std::cout << h.finder(array, 3);
	return 0;
}