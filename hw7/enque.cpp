﻿#include <iostream>

#include<stack>

class TEnqueue {
private:
	std::stack<int> first;
	std::stack<int> second;
	void function() { 
		if (second.size() == 0) {                                     
			for (int i = first.size(); i > 0; --i) {                
				int num = first.top();
				second.push(num);
				first.pop();
			}
		}
	}
public:
	void Push(int x) {
		first.push(x);
	}
	void Pop() {
		function(); 
		second.pop();
	}
	int Top() {
		function();
		return second.top();
	}
	bool  Empty() {
		return first.empty() && second.empty();
	}
	int Size() {
		return first.size() + second.size();
	}
};
int main() {
	TEnqueue h;
	h.Push(110);
	h.Push(115);
	h.Pop();
    std::cout << h.Top() << " " << h.Size();
}

