#include <iostream>

struct TStackEl {
	int Data;
	TStackEl* Next;
};

class TStack {
	unsigned int len;
	TStackEl* head;
	TStackEl* min;
public:
	TStack() : len(0) {} 
	void Pop() {
		if (len > 0) {
			TStackEl* a = head;
			TStackEl* b = min;
			head = head->Next;
			min = min->Next;
			delete a;
			delete b;
			--len;
		}
		else std::cout << "error";
	}

	int Top() {
		return head->Data;
	}

	void Push(int x) {
		TStackEl* newEl = new TStackEl;
		newEl->Data = x;
		newEl->Next = head;
		if (len == 0) {
			min = newEl;
		}
		else {
			if (*Next < *newEl) {
				TStackEl* a = newEl;
				newEl = Next;
				Next = a;
			}
			min = newEl;
		}
		head = newEl;
		len++;

	}

	bool Empty() {
		return len == 0;
	}

	unsigned int Size() {
		return len;
	}

	size_t Min() {
		return head->Data;
	}
};