#include <iostream>

#include <vector>

using std::vector;

class TMerge {

public:

	void MergeSort(std::vector<int> array) {
		MergeSortInside(array, 0 , array.size() - 1);
	}

private:

	void MergeSortInside(vector<int> array, size_t l, size_t r) {
		if ((r - l) > 0) {
			MergeSortInside(array, l, (r - l) / 2 + l); //l1 , l2
			MergeSortInside(array, (r - l) / 2 + 1, r); // r1, r2
			Merge(array, l, (r - l) / 2 + 1, (r - l) / 2 + l, r); // l1, r1, l2, r2
		}
	}
	void Merge(vector<int> array, size_t l1, size_t r1, size_t l2, size_t r2) {
		std::vector<int> mas;
		while (l1 != r1 && l2 != r2) {
			if (array[l2] < array[l1]) {
				mas.push_back(array[l2]);
				++l2;
			}
			else {
				mas.push_back(array[l1]);
				++l1;
			}
		}
		while(l1 <= r1 && l2 == r2) {
			mas.push_back(array[l1]);
			++l1;
		}
		while (l2 <= r2 && l1 == r1) {
			mas.push_back(array[l2]);
			++l2;
		}
		array.clear();
		array.insert(mas.begin(), mas.end());
	}
};

int main() {

	std::vector<int> array;
	int i, k;
	TMerge m;
	std::cin >> "Enter size of array: " >> " " >> k;
	for (int i = 0; i < k; ++i) {
		int j;
		std::cin >> j;
		array.push_back(j);
	}
	m.MergeSort(array);
	for (int i = 0; i < k;  ++i) {
		std::cout << array[i];
	}
	return 0;
}