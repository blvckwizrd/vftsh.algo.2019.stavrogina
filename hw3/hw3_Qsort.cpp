#include <iostream>
#include <vector>
#include<algorithm>

class TQSort {
public:
	void QSort(std::vector<int> array) {
		QSortInside(array, array.size() - 2, 0, --array.size()); //������� ������� pivot - ������� ������
	}
private:
	void QSortInside(std::vector<int>array, size_t r, size_t l, size_t pivot) {
		if (array.size() > 1) {
			Sort(array, pivot, r, l);
			QSortInside(array, --pivot, l, pivot);
			QSortInside(array, r - 2, 0, --r);// ��-����� ����� �������� �������� ��� �������� �������� ������ .
		}
	}
	void Sort(std::vector<int> array, size_t pivot, size_t r, size_t l) {
		while (r >= l) {
			while (array[l] < array[pivot]) {
				++l;
			}
			while (array[r] > array[pivot]) {
				--r;
			}
			if (array[l] > array[pivot] && array[r] < array[pivot]) {
				std::swap(array[l], array[r]);
			}
		}
		if (l < r && array[pivot] > array[r]) {
			std::swap(array[pivot], array[r]);
		}
	}
};
*/
int main() {
	std::vector<int> array;
	int k;
	std::cin >> "Enter size of array: " >> " " >> k;
	for (int i = 0; i < k; ++i) {
		int j;
		std::cin >> j;
		array.push_back(j);
	}
	QSort(array);
	for (int i = 0; i < k; ++i) {
		std::cout << array[i];
	}
	return 0;
}