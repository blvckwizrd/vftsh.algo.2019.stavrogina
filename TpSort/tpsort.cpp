﻿// tpsort.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include<unordered_set>
#include <unordered_map>
#include <map>
#include<stack>
struct Vertex {
	int data, color = 0;
	std::unordered_set<int> neighbors;
};
class TpSort {
	std::unordered_map<int, Vertex> graph;
	std::stack<Vertex> vertices;
	void dfs(Vertex&vertex) {
		vertex.color = 1;
		//std::cout << "vertex int dfs : " << vertex.data << '\n';
		for (auto it = vertex.neighbors.begin(); it != vertex.neighbors.end(); ++it) {
			if (graph.find(*it)->second.color == 0)
				dfs(graph.find(*it)->second);
		}
		vertex.color = 2;
		//std::cout << "vertex in stack : " << vertex.data << '\n';
		vertices.push(vertex);
	}
public:
	void Push(int x) {
		int count;
		Vertex El;
		El.data = x;
		std::cout << "enter  number of vertexe's neighbors: " << '\n';
		std::cin >> count;
		for (int i = 0; i < count; ++i) {
			std::cout << "enter neighbor: " << '\n';
			int neighbor;
			std::cin >> neighbor;
			El.neighbors.insert(neighbor);
		}
		graph.insert(std::make_pair(x, El));
	}
	void Sort() {
		for (auto it = graph.begin(); it != graph.end(); ++it) {
			if (it->second.color == 0) dfs(it->second);
		}

	}
	void Show() {
		//std::cout << vertices.size() << '\n';
		for (; !vertices.empty();) {
			std::cout << vertices.top().data << '\n';
			vertices.pop();
		}
	}
};
int main() {
	TpSort h;
	std::cout << "enter count of vertices: " << '\n';
	int count;
	std::cin >> count;
	for (int i = 0; i < count; ++i) {
		std::cout << "enter vertex: " << '\n';
		int vertex;
		std::cin >> vertex;
		h.Push(vertex);
	}
	h.Sort();
	h.Show();
	return 0;
}