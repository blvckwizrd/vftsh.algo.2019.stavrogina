#include <iostream>
#include <fstream>
#include<unordered_map>
#include < string>

void Count(std::string word, std::unordered_map<std::string, int> *text) {
    auto key = text->find(word);
    if (key != text->end())++key->second;
    else text->insert(std::pair<std::string, int>(word, 1));
}
int main() {
    std::unordered_map<std::string, int> counter;
    std::string word = "", text, filename;
    auto* j = &counter;
    std::wcout << "input filename: " << '\n';
    std::cin >> filename;
   //text = "Bach enriched established German styles through his mastery of counterpoint, harmonic and motivic organisation, and his adaptation of rhythms, forms, and textures from abroad, particularly from Italy and France.";
    std::ifstream in(filename);
    if (in.is_open()) {
        while (getline(in, text)) {
            std::cout << text << std::endl;
        }
    }
    in.close();
    for (auto it = text.begin(); it != text.end(); ++it) {
        if (*it == ' ' || *it == ',' || *it == '.' || *it == '!' || *it == '?') {
            if (word != "") {
                Count(word, j);
                //std::cout << word << '\n';
            }
            word = "";
        }
        else {
            std::cout << *it << '\n';
            word.push_back(*it);
        }
    }
    for (auto it = counter.begin(); it != counter.end(); ++it) {
        std::cout << it->first << " : " << it->second << std::endl;
    }
    return 0;
}