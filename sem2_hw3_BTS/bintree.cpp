﻿// bintree.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
//it's set 
#include <iostream>
#include<list>
struct TBinTreeEl {
	int data;
	TBinTreeEl* left;//указатель на левого потомка
	TBinTreeEl* right;//указатель на правого потомка
	TBinTreeEl* parent;//указатель на родителя
};
class TBinTree {
	TBinTreeEl* root;//корень дерева
	unsigned int len = 0;//размер
	TBinTreeEl* shiftDown(int x, TBinTreeEl* parent) {
		if (x >= parent->data) {
			if (parent->right == nullptr) return parent;
			return shiftDown(x, parent->right);
		}
		else {
			if (parent->left == nullptr) return parent;
			return shiftDown(x, parent->left);
		}
	}
	void shiftForShow(TBinTreeEl* element) {
		if (element != nullptr) {
			std::cout << "element: " << element->data << '\n';
			if (element->right != nullptr) {
				std::cout << "element right: " << element->data << " " << element->right->data << '\n';
				shiftForShow(element->right);
			}
			if (element->left != nullptr) {
				std::cout << "element left : " << element->data << " " << element->left->data << '\n';
				shiftForShow(element->left);
			}
		}
	}
	TBinTreeEl* Search(int x, TBinTreeEl* element) {
		if (x == element->data) return element;
		else if (x > element->data) {
			if (element->right == nullptr) return element;
			return Search(x, element->right);
		}
		else if (x < element->data) {
			if (element->left == nullptr)return element;
			return Search(x, element->left);
		}
	}
public:
	void Push(int x) {//okay
		if (len == 0) {
			TBinTreeEl* newEl = new TBinTreeEl;
			newEl->data = x;
			newEl->parent = nullptr;
			newEl->right = nullptr;
			newEl->left = nullptr;
			root = newEl;
		}
		else {
			if (!Find(x)) {
				TBinTreeEl* newEl = new TBinTreeEl;
				TBinTreeEl* ancestor = shiftDown(x, root);
				newEl->parent = ancestor;
				newEl->data = x;
				newEl->left = nullptr;
				newEl->right = nullptr;
				if (ancestor->data >= x) {
					ancestor->left = newEl;
				}
				if (ancestor->data < x) {
					ancestor->right = newEl;
				}
			}
		}
		++len;
	}
	bool Find(int x){//okay
		if (Search(x, root)->data != x) return false;
		else return true;
	}
	void Delete(int x) {//okay
		auto element = Search(x, root);
		if (element->right == nullptr && element->left == nullptr) {
			if (len > 1) {
				if (element->parent->data > element->data) delete element->parent->left;
				delete element->parent->right;
			}
			else delete element;
		}
		else if (element->right != nullptr && element->left != nullptr) {
			auto minimum = Search(x + 1, element);
			element->data = minimum->data;
			if (minimum->parent->data > minimum->data) minimum->parent->left = nullptr;
			else minimum->parent->right = nullptr;
			delete minimum;
		}
		else {
			TBinTreeEl* key;
			if (element->right != nullptr) {
				key = element->right;
			}
			else {
				key = element->left;
			}
			if (element->parent->data > element->data) {
				element->parent->left = key;
				key->parent = element->parent;
			}
			else {
				element->parent->right = key;
				key->parent = element->parent;
			}
			delete element;
		}
		--len;
	}
	void Show() {
		shiftForShow(root);
	}
	int Size() {//okay
		return len;
	}
};
int main() {
	TBinTree h;
	h.Push(15);
	h.Push(36);
	h.Push(10);
	h.Push(14);
	h.Push(5);
	h.Push(21);
	h.Push(20);
	h.Push(13);
	h.Push(4);
	h.Push(6);
	h.Push(16);
	h.Push(18);
	std::cout << "size: " << h.Size() << '\n';
	h.Show();
	std::cout << h.Find(18) << " " << h.Find(20) << '\n';
	std::cout << h.Find(10) << " " << h.Find(12) << '\n';
	h.Delete(20);
	h.Delete(16);
	h.Delete(15);
	std::cout << "size: " << h.Size() << '\n';
	h.Show();
	std::cout << h.Find(18) << " " << h.Find(20) << '\n';
	return 0;
}
// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
