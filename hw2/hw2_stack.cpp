#include <iostream>

struct TStackEl {
	int Data;
	TStackEl* Next;
};

class TStack {
	unsigned int Len;
	TStackEl* Head;
public:
	TStack() : Len(0) {} 
	void Pop() {
		if (Len > 0) {
			TStackEl* a = Head;
			Head = Head->Next; 
			delete a;
			--Len;
		}
		else std::cout << "error";
	}

	int Top() {
		return Head->Data;
	}

	void Push(int x) {
		TStackEl* newEl = new TStackEl;
		newEl->Data = x;
		newEl->Next = Head;
		Head = newEl;
		Len++;
	}

	bool Empty() {
		return Len == 0;
	}

	unsigned int Size() {
		return Len;
	}

};

