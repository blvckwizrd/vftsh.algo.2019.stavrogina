#include <iostream>

struct TDequeEl {
	int Data;
	TDequeEl* Next;
	TDequeEl* Previous;
};

class TDeque {
	unsigned int Len;
	TDequeEl* Head;
	TDequeEl* Back;
public:
	TDeque() : Len(0) {}

	void PopFront() {
		TDequeEl* a = Head;
		Head = Head->Next;
		if (Len > 1) {
			//TDequeEl* b = Back;
			Head->Previous = nullptr;
		}
		delete a;
		--Len;
	}
	void PopBack() {
		TDequeEl* a = Back;
		Back = Back->Previous;
		if (Len > 1) {
			//TDequeEl* b = Back;
			Back->Next = nullptr;
		}
		delete a;
		--Len;
	}
	void PushBack(int x) {
		TDequeEl* newEl = new TDequeEl;
		newEl->Data = x;
		newEl->Previous = Back;
		if (Len > 0) {
			//TDequeEl* b = Back;
			Back->Next = newEl;
		}
		Back = newEl;
		if (Len == 0)Head = newEl;
		++Len;
	}
	void PushFront(int x) {
		TDequeEl* newEl = new TDequeEl;
		newEl->Data = x;
		newEl->Next = Head;
		if (Len > 0) {
			//TDequeEl* b = Head;
			Head->Previous = newEl;
		}
		if (Len == 0)Back = newEl;
		Head = newEl;
		++Len;
	}
	bool Empty() {
		return Len == 0;
	}
	unsigned int Size() {
		return Len;
	}
	int Top() {
			return Head->Data;
	}
	int Tail() {//ругается, нельзя Back назвать
			return Back->Data;
	}
};

int main() {
	TDeque h;
	h.PushBack(125);
	//std::cout << h.Tail() << std::endl;
	//h.PopBack();
	std::cout << h.Size() << " " << h.Tail() << std::endl;
	h.PushFront(130);
	h.PushFront(145);
	h.PushBack(117);
	h.PopBack();
	std::cout << h.Size() << " " << h.Tail() << std::endl;
	h.PushBack(113);
	h.PopFront();
	std::cout << h.Tail() << " " << h.Top() << std::endl;
	return 0;
}