﻿﻿﻿#include <iostream>
#include <clocale>
#include <unordered_map>
using std::string;
using std::unordered_multimap;
class Data {
	unordered_multimap<string, string> data;
public:
	void Add(string Surname, string Name) {
		data.insert(std::pair<string, string>(Surname, Name));
	}
	bool Check(string Surname, string Name) {
		auto key = data.find(Surname);
		if (key != data.end()) {
			while (key == data.find(Surname)) {
				if (key->second == Name) return true;
				++key;
			}
			return false;
		}
		else return false;
	}
    void Delete(string Surname, string Name) {
		if (data.count(Surname) != 0) {
			auto k = data.equal_range(Surname);
			auto it = k.first;
			for (; it != k.second;) {
				if (it->second == Name) {
					auto key = it;
					++it;
					data.erase(key);
				}
				else ++it;
			}
		}
		else std::cout << "error";
	}
	void Show() {
		for (auto it = data.begin(); it != data.end(); ++it) {
			std::cout << it->first << " " << it->second << '\n';
		}
	}
	void Select(string Surname) {
		auto k = data.equal_range(Surname);
		for (auto it = k.first; it != k.second; ++it) {
			std::cout << it->first << " " << it->second << '\n';
		}
	}
};
int main() {
	setlocale(LC_CTYPE, "rus");
	Data h;
	h.Add("Просто", "Санек");
	h.Add("Батиков", "Женя");
	h.Add("Ковальчук", "Никита");
	h.Add("Просто", "Гошан");
	std::cout << h.Check("Горгородов" ,"Оксиминог") << std::endl;
	h.Delete("Батиков", "Женя");
	h.Show();
	h.Select("Просто");
	return 0;
}